FROM python:alpine

RUN apk update
RUN pip install --upgrade pip

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

ENV FLASK_APP=flaskr
ENV FLASK_ENV=development

COPY instance/ .
COPY flaskr/ .

RUN cd flaskr
CMD flask run