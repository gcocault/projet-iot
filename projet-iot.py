#!/usr/bin/env python3

from flask import Flask
from flask import Response
from flask import request


app = Flask(__name__)


@app.route('/')
def home():
    return Response("Bienvenue dans ce projet IOT de qualité supérieure ! Un simple push met à jour le serveur en moins de 5 minutes !")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
